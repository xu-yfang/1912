import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
import "@/styles/common.less"
import uiPlugin from "@/plugin/ui.registry"
import baseComponent from "@/plugin/baseComponent.registry"

createApp(App).use(router).use(uiPlugin).use(baseComponent).mount("#app")
