import BaseHeader from "@/components/header.vue"
import { App } from "vue"
export default{
    install(app:App):void{
        app.component("base-header",BaseHeader)
    }
}
