import { Button, message } from "ant-design-vue"
const install = (app: any) => {
  console.log(app)
  app.use(Button)
  app.config.globalProperties.$message = message
}

export default {
  install,
}
