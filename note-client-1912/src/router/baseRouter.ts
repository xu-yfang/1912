
const baseRouter=[
    {
        path:"/",
        component:()=>import("@/views/article/index.vue"),
        meta:{
            title:"文章",
            nav:true
        }
    },
    {
        path:"/archives",
        component:()=>import("@/views/archives/index.vue"),
        meta:{
            title:"归档",
            nav:true
        }
    },
    {
        path:"/konwladge",
        component:()=>import("@/views/konwladge/index.vue"),
        meta:{
            title:"知识小结",
            nav:true
        }
    },
]

export default baseRouter