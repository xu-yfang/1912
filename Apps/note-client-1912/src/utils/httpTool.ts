import axios from "axios"

export interface PageNationParams {
  page?: number
  pageSize?: number
}
export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined
}

const httpTool = axios.create({
  timeout: 10000,
  baseURL: "",
})

export default {
  ...httpTool,
  get(url: string, params: Params) {
    return httpTool.get(url,{
        params
    })
  },
  post() {
    console.log("post");
    
  },
}
