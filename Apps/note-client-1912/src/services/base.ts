import http from "@/utils/httpTool"
import { Params }  from "@/utils/httpTool"

export const getPage = (params: Params = {}) => http.get("/api/article",params)

