import { defineStore } from "pinia"
import { base } from "@/services"


const useStore = defineStore("baseStore", {
  state: () => ({
    list:[]
  }),
  actions: {
    async getData() {
      const { data } = await base.getPage();
      this.$patch({
        list: data
      })
    },
  },
})

export default useStore
