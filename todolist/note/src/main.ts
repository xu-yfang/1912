import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
// import less-loader from "less-loader"
import "@/styles/common.less"

createApp(App).use(router).mount("#app");
