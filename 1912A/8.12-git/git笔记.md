### git
版本管理工具
命令行工具

检测git 是否安装成功
在电脑管理员页面输入 git version 查看是否安装成功git

## 怎么管理文件
1、git init 
在当前目录下会产生.git文件夹 不允许子目录中有.git 会嵌套管理 管理所有目录

## 怎么提交
-本地工作区(本地文件夹)
-暂存区
-历史版本区 (产生文件提交的版本号)
    以上是本地仓库
-远程仓库 (第三方仓库)

本地工作区 -> (git add 路径/-A) 暂存区 -> (git commit -m 'message提交信息') 历史版本区

历史版本区 -> (git push) 远程仓库


git add . 提交当前目录
git add -A 提交所有目录

git log 查看提交纪录
git help 可以查看git 别名

##  可视化工具 
Sourcetree

##  退出打印
Esc+:+q 

##  回滚文件状态
init -> docs: upd(head) -> upd: index.js

1、git reset  直接回到回滚到的commitId,之前所有的纪录不显示
git reset --hard HEAD^  回滚到上一个状态
git reset --hard HEAD^^  回滚到上两个状态
git reset --hard HEAD^^^  回滚到上三个状态
git reset --hard HEAD～6  回滚到上六个状态
git reset --hard HEAD～n   回滚到上n个状态
git reset --hardcommitId   回滚到上某个状态

2、git revert  新生成一个commit 可以两个对比 不会替换之前得纪录  
git revert --no-commit + commitId  

##  增加一个状态
git add -A 
git commit -m '信息'
git log   即可查看新增的状态

## 推送
1、本地仓库跟远程仓库关联
本地仓库已经存在, 通过git remote add origin 仓库地址 关联远程仓库
本地仓库不存在 直接克隆远程仓库 自动关联 

## 查看文件状态
git status


http
ssh协议  需要配置秘钥




提交代码
<<<<<<< HEAD

##  多人协作开发

一、解决冲突
碰到冲突 解决冲突之后在此提交

使用分支进行开发
创建开发分支 多人开发 创建自身开发分支 development
测试分支  test
预发分支  release
合到主分支 master

1、创建分支 
shell
git branch branchname

2、查看本地分支
git branch
  dev_xyf_8.13
* master  星号代表目前所在分支

3、查看所有分支
shell
git branch --all

4、查看远程分支
git branch -r

切换分支
git checkout dev_xyf_8.13    执行之后代码状态返回到分支创建之前

创建并切换分支
git checkout -b branchname
git checkout -b dev_xyf_8.13 

合并分支
git merge 要合并的分支代码
git merge master
=======
提交代码
提交代码
111111
11111
2222
>>>>>>> feb995d890c5257fb3cd1b9889f5d2214123ce33
